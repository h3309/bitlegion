import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import {useHistory} from 'react-router-dom'
import { Paper } from '@mui/material';
import { useState } from 'react';
import { auth } from '../component/auth.services'

const theme = createTheme();


export default function Login() {
    const history = useHistory()
    const [role, setRole] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

  const checkUsernamePasswordAndRole = () =>{
    let authenticated = false
    if(email === 'bitlegionbusinesspartner1@gmail.com' && password === '123'){
      auth.setRole("Partner")
      authenticated = true
    }
    else if(email === 'tlavote1@gmail.com' && password === '123'){
      auth.setRole('Analyst')
      authenticated = true
    }
    else if(
      email === 'abc123@gmail.com' && password === '123'){
        auth.setRole('Controller')
        authenticated = true
    }

    if(authenticated){
      history.push('/dashboard')
    }

  }

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="sm"  >
        <Paper style={{width: '70vh', height: '50vh'}} elevation={5}>
        <Box
          sx={{
            marginTop: 20,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Typography component="h1" variant="h5" style={{paddingTop: 20}}>
           Bit Legion Inventory system
          </Typography>
          <Avatar sx={{ m: 1, bgcolor: '#0076CE' }}>
            <LockOpenIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              value={email}
              onChange={(e)=>{
                setEmail(e.target.value)
              }}
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              value={password}
              onChange={(e)=>{
                setPassword(e.target.value)
              }}
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              style={{backgroundColor: '#0076CE', marginTop: 30}}
              onClick={checkUsernamePasswordAndRole}
            >
              Sign In
            </Button>
          </Box>
        </Box>
        </Paper>
      </Container>
    </ThemeProvider>
  );
}
