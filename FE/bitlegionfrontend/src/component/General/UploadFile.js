import React, { useState } from 'react';
import axios from 'axios'


export const UploadFile = () =>{
    const [file, setFile] = useState(null)
    const onInputChange = (e) =>{
        console.log("WHAT IS VALUE", e.target.files[0])
        setFile(e.target.files[0])
    }
    const onSubmit = (e) =>{
        e.preventDefault()
        const data = new FormData();
        data.append('file', file);
        data.append('name', 'Test Name');
        data.append('desc', 'Test description');
        // fetch("http://localhost:3001/todo/upload", {
        //      method: 'POST',
        //      headers: {
        //          'Accept': 'application/json',
        //      },
        //      body: data
        // }).then((response) =>  {
        //    return response.text();
        // })

        // OR 
        axios.post('URL', data).then((e)=>{

        }).catch((e)=>{
            console.log('ERROR')
        })
    }
    return(
        <form method='post' action='#' id='#' onSubmit={onSubmit}>
            <div>
                <label>Upload Your File</label>
                <input type={'file'} 
                onChange={onInputChange}
                className={'form-control'} multiple={false} />
            </div>
            <button> submit</button>
        </form>
    )
}