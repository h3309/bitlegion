import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

export default function ContentLongCard(props) {
  return (
    <div style={{alignItems: 'center', justifyContent: 'center',}}>
    <Card sx={{ width: 280, alignItems: 'center', justifyContent:'center', boxShadow: "2px 2px 5px #898989" }}>
      <CardContent>
        <Typography gutterBottom variant="body2" component="div">
          {props.title}
        </Typography>
        {props.list.map((item)=>{
          return(
              <div style={{marginBottom: 10}}>
          <Typography variant="body3" color="text.secondary" >
          {item}
        </Typography>
        </div>)
        })}
      </CardContent>
    </Card>
    </div>
  );
}
