import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PeopleIcon from '@mui/icons-material/People';
import BarChartIcon from '@mui/icons-material/BarChart';
import LayersIcon from '@mui/icons-material/Layers';
import AssignmentIcon from '@mui/icons-material/Assignment';

export const mainListItems = (
  <>
    <ListItemButton style={{ width: '100%', paddingLeft: 10, paddingTop: 10, paddingBottom: 10, color: "#41B6E6" }}>
      <ListItemIcon>
        <DashboardIcon htmlColor='#41B6E6'/>
      </ListItemIcon>
      <ListItemText primary="Dashboard"  />
    </ListItemButton>
  </>
);