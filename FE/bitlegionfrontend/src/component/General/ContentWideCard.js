import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import AllInboxIcon from '@mui/icons-material/AllInbox';

export default function ContentWideCard(props) {
  return (
    <div style={{alignItems: 'center', justifyContent: 'center'}}>
    <Card sx={{ width: 280, alignItems: 'center', justifyContent:'center', boxShadow: "2px 2px 5px #898989" }}>
      <CardContent style={{display: 'flex', flexDirection:'row'}}>
        {props.children}
        <div>
        <Typography gutterBottom variant="body2" component="div">
          {props.title}
        </Typography>
        <Typography variant="h5" color="text.secondary">
          {props.number}
        </Typography>
        </div>
      </CardContent>
    </Card>
     </div>
  );
}
