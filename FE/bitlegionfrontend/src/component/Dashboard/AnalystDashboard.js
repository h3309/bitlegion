import React from 'react'
import ContentWideCard from '../General/ContentWideCard'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button, Chip } from '@mui/material';
import PaidIcon from '@mui/icons-material/Paid';
import CategoryIcon from '@mui/icons-material/Category';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';

export const AnalystDashboard = () =>{

  function createData(name, id, email, status, color) {
    return { name, id, email, status, color };
  }
  const rows = [
    createData('Mavis', 15645,'bitlegionbusinesspartner1@gmail.com', 'In Review', "warning"),
    createData('Mavis', 15295,'bitlegionbusinesspartner1@gmail.com',  'In Review', "warning")
  ];
  
    return(
     <div style={{marginLeft: 20, marginBottom: 100}}>
            <p style={{fontSize: 48, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
          Dashboard
          </p>
        <div style={{marginTop: 20}}>
          <p style={{fontSize: 36, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
            Overview
            </p>
        </div>
        <div style={{display: "flex", flexDirection: 'row'}}>
          <ContentWideCard title={"Total Sales per Week"} number = {200}>
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', paddingRight: 10}}>
        <PaidIcon style={{fontSize: 50}} htmlColor={'#F2AF00'}/>
        </div> 
             </ContentWideCard>
          <div style={{marginLeft: 30}}>
          <ContentWideCard title={"Best Category Product"} number = {200}>
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', paddingRight: 10}}>
        <CategoryIcon style={{fontSize: 50}} htmlColor={'#6E2585'}/>
        </div> 
          </ContentWideCard>
          </div>
          <div style={{marginLeft: 30}}>
          <ContentWideCard title={"Best Product Item"} number = {200}>
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', paddingRight: 10}}>
        <ShoppingCartIcon style={{fontSize: 50}} htmlColor={'#00447C'}/>
        </div> 
             </ContentWideCard>
          </div>
          </div>
          <div>
          <p style={{fontSize: 36, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
            Recent Inventory Stock
            </p>
            <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Business Partner Name</TableCell>
            <TableCell> Confirmation ID</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Status</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell>{row.id}</TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell><Chip label={row.status} color={row.color}/></TableCell>
              <TableCell>
                <Button variant='contained' onClick={(e)=>{
                  console.log('GENERATE REPORT', row.name)
              }} style={{backgroundColor: '#0076CE'}}> Verify </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
          </div>
    </div>
    )
}