import { Button, Chip } from '@mui/material'
import React from 'react'
import ContentWideCard from '../General/ContentWideCard'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import AllInboxIcon from '@mui/icons-material/AllInbox';
import PersonPinIcon from '@mui/icons-material/PersonPin';
import VerifiedIcon from '@mui/icons-material/Verified';

export const ControllerDashboard = () =>{

  function createData(name, id, email, status, color) {
    return { name, id, email, status, color };
  }
  function createData1(name, email) {
    return { name, email };
  }
  const rows = [
    createData('Mavis', 15645,'bitlegionbusinesspartner1@gmail.com', 'Verified', "success"),
    createData('Mavis', 15645,'bitlegionbusinesspartner1@gmail.com', 'In Review', "warning"),
    createData('Mavis', 15295,'bitlegionbusinesspartner1@gmail.com',  'Rejected', "error")
  ];

  const rowsGenerate = [
    createData1('Mavis', 'bitlegionbusinesspartner1@gmail.com')
  ];
  
    return(
     <div style={{marginLeft: 20, marginBottom: 100}}>
            <p style={{fontSize: 48, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
          Dashboard
          </p>
        <div style={{marginTop: 20}}>
          <p style={{fontSize: 36, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
            Inventory Overview
            </p>
        </div>
        <div style={{display: "flex", flexDirection: 'row'}}>
          <ContentWideCard title={"Total Available Products"} number = {200}> 
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', paddingRight: 10}}>
        <AllInboxIcon style={{fontSize: 50}} htmlColor={'#41B6E6'}/>
        </div> </ContentWideCard>
          <div style={{marginLeft: 30}}>
          <ContentWideCard title={"Total Business Partner"} number = {3} >
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', paddingRight: 10}}>
        <PersonPinIcon style={{fontSize: 50}} htmlColor={'#6E2585'}/>
        </div>
             </ContentWideCard>
          </div>
          <div style={{marginLeft: 30}}>
          <ContentWideCard title={"Total Verified Stock"} number = {1}>
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', paddingRight: 10}}>
        <VerifiedIcon style={{fontSize: 50}} htmlColor={'#6EA204'}/>
        </div>
             </ContentWideCard>
          </div>
          </div>
          <div style={{marginTop: 30}}>
          
          </div>
          <div>
          <p style={{fontSize: 36, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
            Recent Inventory Stock
            </p>
            <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Business Partner Name</TableCell>
            <TableCell> Confirmation ID</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell>{row.id}</TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell><Chip label={row.status} color={row.color}/></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
          </div>
          <div>
          <p style={{fontSize: 36, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
            Recent Report
            </p>
          </div>
          <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Business Partner Name</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rowsGenerate.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell>
                <Button variant='contained' onClick={(e)=>{
                  console.log('GENERATE REPORT', row.name)
              }} style={{backgroundColor: '#0076CE'}}> Generate Report </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
    )
}