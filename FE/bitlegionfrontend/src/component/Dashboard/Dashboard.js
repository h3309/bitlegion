import React from 'react'
import CombineBar from '../General/CombineBar'
import { AnalystDashboard } from './AnalystDashboard'
import { BPDashboard } from './BPDashboard'
import { ControllerDashboard } from './ControllerDashboard'
import {auth} from '../auth.services'

export const Dashboard = () =>{
    const user = auth.getRole()
    switch (user){
        case 'Controller':
            return(
            <CombineBar>
                <ControllerDashboard /> 
            </CombineBar>
            )
        case 'Analyst':
            return(
             <CombineBar>
                 <AnalystDashboard />
            </CombineBar>
            )
        default:
            return(
                <CombineBar>
                    <BPDashboard />
                </CombineBar>
            ) 
    }
}