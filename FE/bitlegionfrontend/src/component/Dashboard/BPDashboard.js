import { Chip, Divider,Typography } from '@mui/material'
import React, { useState } from 'react'
import ContentWideCard from '../General/ContentWideCard'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button } from '@material-ui/core'
import UploadFileIcon from '@mui/icons-material/UploadFile';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import AllInboxIcon from '@mui/icons-material/AllInbox';

export const BPDashboard = () =>{
  const [show, setShow] = useState(false);
  const [file, setFile] = useState(null)

  function createData(name, id, email, status, color) {
    return { name, id, email, status, color };
  }
  const rows = [
    createData('Mavis', 15645,'bitlegionbusinesspartner1@gmail.com', 'Verified', "success"),
    createData('Mavis', 15645,'bitlegionbusinesspartner1@gmail.com', 'In Review', "warning"),
    createData('Mavis', 15295,'bitlegionbusinesspartner1@gmail.com',  'Rejected', "error")
  ];
  const handleClose =() =>{
    setShow(false)
  }
  const onInputChange = (e) =>{
    console.log("WHAT IS VALUE", e.target.files[0])
    setFile(e.target.files[0])
}

  const handleSubmit = (e) =>{
    e.preventDefault()
    const data = new FormData();
    data.append('file', file);
    data.append('name', 'Test Name');
    data.append('desc', 'Test description');
    // fetch("http://localhost:3001/todo/upload", {
    //      method: 'POST',
    //      headers: {
    //          'Accept': 'application/json',
    //      },
    //      body: data
    // }).then((response) =>  {
    //    return response.text();
    // })

    // OR 
    // axios.post('URL', data).then((e)=>{

    // }).catch((e)=>{
    //     console.log('ERROR')
    // })
}
  
    return(
     <div style={{marginLeft: 20, marginBottom: 100}}>
            <p style={{fontSize: 48, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
          Dashboard
          </p>
        <div style={{marginTop: 20}}>
          <p style={{fontSize: 36, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
            Inventory Overview
            </p>
        </div>
        <div style={{display: "flex", flexDirection: 'row'}}>
          <ContentWideCard title={"Quantity Stock on Hand"} number = {200}>
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center', paddingRight: 10}}>
        <AllInboxIcon style={{fontSize: 50}} htmlColor={'#41B6E6'}/>
        </div> 
             </ContentWideCard>
          </div>
        <div style={{marginTop: 100}}>
          <div>
          <Typography style={{fontSize: 36, fontFamily:'sans-serif', fontWeight: 'lighter'}}>
            Recent Inventory Stock
            </Typography>
          </div>
          <div style={{display:'flex', justifyContent:'flex-end', marginRight: 10, marginBottom: 20}}>
            <Button variant='contained' style={{backgroundColor: '#0076CE',color: '#ffff'}} onClick={()=>{setShow(true)}}>
              <UploadFileIcon style={{marginRight: 7}} />
               Update Stock</Button>
               <Dialog open={show} onClose={handleClose}>
        <DialogTitle>Stock Update
        </DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText>
            <div style={{width: '50vh', height: '20vh'}}>
          <label>Subject: Update Inventory Stock</label>
          <div style={{paddingTop: 20}}>
                <input type={'file'} accept={".csv,.xlsx, .pdf, .doc, .docx" } 
                onChange={onInputChange}
                className={'form-control'} multiple={false} />
            </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions style={{marginBottom:15}}>
          <Button variant={'contained'} color={'error'} onClick={handleClose} style={{marginRight: 10}} >Cancel</Button>
          <Button variant={'contained'} onClick={handleSubmit} style={{backgroundColor: '#0076CE', color: 'white', marginRight: 10}}>submit</Button>
        </DialogActions>
      </Dialog>
          </div>
          <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Business Partner Name</TableCell>
            <TableCell> Confirmation ID</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell>{row.id}</TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell><Chip label={row.status} color={row.color}/></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
          </div>
    </div>
    )
}