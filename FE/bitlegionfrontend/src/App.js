import logo from './logo.svg';
import './App.css';
import Login from './component/Login';
import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import { Dashboard } from './component/Dashboard/Dashboard';
import { Inbox } from './component/Inbox';

function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Login} />
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/inbox" component={Inbox} />
      <Route path="*" component={<></>}/>

    </Switch>
    </BrowserRouter>


  );
}

export default App;
